import React from "react";
import {Link} from "react-router-dom";
import CardForCartPage from "./CardForCartPage";
import Container from "react-bootstrap/Container";
import {useSelector} from "react-redux";
import OrderForm from "./OrderForm";


function CartPage() {
    const productList = useSelector(state => state.products.items);
    const addedToCart = useSelector(state => state.cart.items)
    const filteredProducts = productList.filter(({id}) => addedToCart.some(product => id === product.id));
    const productsInCart = filteredProducts.map(product => {
        const index = addedToCart.findIndex(({id}) => id === product.id);
        return {...product, inCart: addedToCart[index].count}
    })

    let priceTotal = 0;
    productsInCart.forEach(product => priceTotal = priceTotal + product.inCart * parseInt(product.price));

    if (addedToCart.length === 0) {
        return (<div className="d-flex flex-column align-items-center justify-content-center vh-100">
            <p className="fs-3"><span className="text-danger">Opps!</span> Your Cart is empty!</p>
            <Link to="/home" className="btn btn-info text-light">Go Home</Link>
        </div>)
    }
    return (
        <Container className='h-100'>
            <h2 className="h2 m-4">Shopping Cart</h2>
            {productsInCart.map((product) => {
                return (<CardForCartPage key={product.id}
                                         product={product}/>)
            })}
            <h2 className="h2 m-4">Total: {priceTotal} USD </h2>
            <OrderForm/>
        </Container>
    )
}

export default CartPage;