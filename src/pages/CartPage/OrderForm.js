import React from 'react';
import {Formik, Form, Field} from 'formik';
import {PatternFormat} from 'react-number-format';
import * as Yup from 'yup';
import {useDispatch, useSelector} from "react-redux";
import {clearCart} from "../../state/actions/cart";
import { useNavigate } from "react-router-dom";


const OrderSchema = Yup.object().shape({
    firstName: Yup.string().trim().matches(/^[a-zA-Z]+$/, "Only letters allowed")
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    lastName: Yup.string().matches(/^[a-zA-Z]+$/, "Only letters allowed")
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    age: Yup.number()
        .typeError('Age must be a number')
        .positive('Too Long!')
        .integer('Too Long!')
        .min(18, 'Call your mommy')
        .max(100, 'Say hi to Satana')
        .required('Required'),
    street: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    city: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    phone:Yup.string()
        .min(14, 'Too Short!')
        .required('Required'),
})

function OrderForm() {
    const addedToCart = useSelector(state => state.cart.items);
    const dispatch = useDispatch();
    let navigate = useNavigate();
    return (
        <Formik initialValues={{
            firstName: '',
            lastName: '',
            age: '',
            street: '',
            city: '',
            phone: ''
        }}
                validationSchema={OrderSchema}
                onSubmit={(values, {resetForm}) => {
                    console.log(addedToCart, values);
                    dispatch(clearCart());
                    resetForm();
                    return navigate('/home');
                }}>
            {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleReset
              }) => (
                <Form className="row g-3"

                      onReset={handleReset}>
                    <h2 className="h2 m-4"> Complete your order </h2>
                    <div className="col-md-6">
                        <label htmlFor="firstName" className="form-label">First Name</label>
                        <Field
                            type="text"
                            className={errors.firstName && touched.firstName ? 'is-invalid form-control' : 'form-control'}
                            id="firstName"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.firstName}/>
                        {errors.firstName && touched.firstName ? (
                            <div className="invalid-feedback d-block">{errors.firstName}</div>
                        ) : null}
                    </div>
                    <div className="col-md-4">
                        <label htmlFor="lastName" className="form-label">Last Name</label>
                        <Field
                            type="text"
                            className={errors.lastName && touched.lastName ? 'is-invalid form-control' : 'form-control'}
                            id="lastName"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.lastName}/>
                        {errors.lastName && touched.lastName ? (
                            <div className="invalid-feedback d-block">{errors.lastName}</div>
                        ) : null}
                    </div>
                    <div className="col-md-2">
                        <label htmlFor="age" className="form-label">Age</label>
                        <Field type="text"
                               className={errors.age && touched.age ? 'is-invalid form-control' : 'form-control'}
                               id="age"
                               onChange={handleChange}
                               onBlur={handleBlur}
                               value={values.age}/>
                        {errors.age && touched.age ? (
                            <div className="invalid-feedback d-block">{errors.age}</div>
                        ) : null}
                    </div>
                    <div className="col-12">
                        <label htmlFor="inputAddress" className="form-label">Address</label>
                        <Field type="text"
                               className={errors.street && touched.street ? 'is-invalid form-control' : 'form-control'}
                               id="street"
                               placeholder="1234 Main St"
                               onChange={handleChange}
                               onBlur={handleBlur}
                               value={values.street}
                        />
                        {errors.street && touched.street ? (
                            <div className="invalid-feedback d-block">{errors.street}</div>
                        ) : null}
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="inputCity" className="form-label">City</label>
                        <Field type="text"
                               className={errors.city && touched.city ? 'is-invalid form-control' : 'form-control'}
                               id="city"
                               onChange={handleChange}
                               onBlur={handleBlur}
                               value={values.city}/>
                        {errors.city && touched.city ? (
                            <div className="invalid-feedback d-block">{errors.city}</div>
                        ) : null}
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="inputPhone" className="form-label">Phone</label>
                        <PatternFormat id='phone'
                                       type="text"
                                       className={errors.phone && touched.phone ? 'is-invalid form-control' : 'form-control'}
                                       onChange={handleChange}
                                       onBlur={handleBlur}
                                       value={values.phone}
                                       format="(###)###-##-##"/>
                        {errors.phone && touched.phone ? (
                            <div className="invalid-feedback d-block">{errors.phone}</div>
                        ) : null}
                    </div>
                    <div className="col-12">
                        <button type="submit" className="btn btn-info" name="submit">Complete order</button>
                        <button type="reset" className="btn btn-warning mx-2" name='reset'>Reset</button>
                    </div>
                </Form>
            )}
        </Formik>
    )
}

export default OrderForm;