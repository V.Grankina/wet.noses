import React,  {useContext}  from 'react';
import Container from "react-bootstrap/Container";
import {Link} from "react-router-dom";
import { useSelector} from "react-redux";
import ProductCard from "../../components/main/Card";
import LayoutChanger from "../../components/main/LayoutChanger";
import CardInline from "../../components/main/CardInline";
import ToggleLayoutContext from "../../ToggleLayoutContext";

function FavoritePage() {
    const products = useSelector(state => state.favoriteProducts.items)
    const productList = useSelector(state => state.products.items);
    const context = useContext(ToggleLayoutContext);
    const inlineLayout = context.inlineLayout;

    if (products.length === 0) {
        return (<div className="d-flex flex-column align-items-center justify-content-center vh-100">
            <p className="fs-3"><span className="text-danger">Opps!</span> There is nothing here!</p>
            <Link to="/home" className="btn btn-info text-light">Go Home</Link>
        </div>)
    }
    const favoriteProducts = productList.filter( product => products.some(value=> value===product.id) )
    return (<Container>
            <>
                <LayoutChanger/>
                <div className="d-flex justify-content-between flex-wrap">
                    {inlineLayout ? favoriteProducts.map(product => {
                        return <CardInline
                            key={product.id}
                            product={product}
                        />
                    }) : favoriteProducts.map(product => {
                        return <ProductCard
                            key={product.id}
                            product={product}
                        />
                    })
                    }
                </div>
            </>
        </Container>
    )
}

export default FavoritePage;