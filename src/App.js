import React, {useEffect, useState} from 'react';
import {Navigate, Route, Routes} from "react-router-dom";
import Container from 'react-bootstrap/Container';
import ProductsList from './components/main/ProductsList';
import Banner from "./components/main/Banner";
import ModalWindow from "./components/Modal";
import CartPage from "./pages/CartPage/Cartpage";
import FavoritePage from "./pages/FavoritePage/Favoritepage";
import NotFoundPage from "./pages/NotFoundPage";
import Layout from "./components/Layout";
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "./state/actions/products";
import {setFavoriteProducts} from "./state/actions/favoriteProducts";
import {setCart} from "./state/actions/cart";
import ToggleLayoutContext from "./ToggleLayoutContext";


function App() {
    const dispatch = useDispatch();
    const isLoading = useSelector(state => state.products.isFetching);
    const isError = useSelector(state => state.products.isError);
    const error = useSelector(state => state.products.error);
    const [inlineLayout, setInlineLayout] = useState(false);
    const handleSetInlineLayout = () => {
        setInlineLayout(true)
    };
    const handleSetGridLayout = () => {
        setInlineLayout(false)
    };

    useEffect(() => {
            dispatch(fetchProducts())
            const favoriteProductsNew = JSON.parse(localStorage.getItem('favoriteProducts'));
            if (favoriteProductsNew) {
                dispatch(setFavoriteProducts(favoriteProductsNew))
            }
            const addedtoCartNew = JSON.parse(localStorage.getItem('addedToCart'));
            if (addedtoCartNew) {
                dispatch(setCart(addedtoCartNew));
            }
        },
        [dispatch])
    if (isError) {
        return <div>Error: {error.message}</div>
    }

    return (
            <ToggleLayoutContext.Provider value={{inlineLayout, handleSetInlineLayout, handleSetGridLayout}}>
            <Routes>
                <Route path='/' element={<Navigate to='/home'/>}/>
                <Route path='/'
                       element={<Layout/>}>
                    <Route path="/home" element={
                        <>
                            <Banner/>
                            <Container>
                                {isLoading === true ? null :
                                    <ProductsList/>}
                                <ModalWindow/>
                            </Container>
                        </>
                    }/>
                    <Route path='/cart' element={
                        <>
                            <CartPage/>
                            <ModalWindow/>
                        </>}/>
                    <Route path='/favorite' element={<>
                        <FavoritePage/>
                        <ModalWindow/>
                    </>}/>
                </Route>
                <Route path='*' element={<NotFoundPage/>}/>
            </Routes>
            </ToggleLayoutContext.Provider>

    );
}

export default App;
