import {screen, render} from "@testing-library/react";
import '@testing-library/jest-dom';
import ModalWindow from "./Modal";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";

const mockStore = configureMockStore([thunk]);

describe("Modal window tests", () => {
    it('should render', () => {
        const testStore = mockStore({
            modal: {
                isOpen: false,
                text: "",
            }
        });
        render(<Provider store={testStore}><ModalWindow/></Provider>)
    })
    it("should render the text",
        () => {
            const testStore = mockStore({
                modal: {
                    isOpen: true,
                    text: "Test question",
                }
            });
            render(<Provider store={testStore}><ModalWindow/></Provider>);
            const testQuestion = screen.getByText(/Test question/i);
            expect(testQuestion).toBeInTheDocument();
        })
})