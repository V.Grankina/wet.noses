import ProductsList from "./ProductsList";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";
import {render, screen} from "@testing-library/react";
import '@testing-library/jest-dom';
import React from "react";
import ToggleLayoutContext from "../../ToggleLayoutContext";

const mockStore = configureMockStore([thunk]);


describe('Product List tests', ()=>{
    it('should render', ()=>{
        const testStore =mockStore({
            products: {
                items: [],
                isFetching: false,
                isError: false,
                error: null
            }
        })
        render(
            <Provider store={testStore}>
                <ToggleLayoutContext.Provider value={{inlineLayout:true}}>
                <ProductsList/>
                </ToggleLayoutContext.Provider>
            </Provider>
        )
    })
    it('should render one product', ()=>{
        const testStore =mockStore({
            products: {
                items: [{
                    "id": 1,
                    "name": "Ciays Water Fountain",
                    "price": "50",
                    "image": "/images/1.jpg",
                    "vendor": "341524909",
                    "color": "Gray"
                }],
                isFetching: false,
                isError: false,
                error: null
            },
            favoriteProducts: {
                items: [],
            },
            cart: {
                items: [],
            }
        })
        render(
            <Provider store={testStore}>
                <ToggleLayoutContext.Provider value={{inlineLayout:false}}>
                    <ProductsList/>
                </ToggleLayoutContext.Provider>
            </Provider>
        )
        const products = screen.getAllByTestId('card');
        expect(products.length).toBe(1)
    })
})