import React, {useContext} from "react";
import ToggleLayoutContext from '../../ToggleLayoutContext';
import {useSelector} from "react-redux";
import LayoutChanger from "./LayoutChanger";
import ProductCard from './Card';
import CardInline from "./CardInline";

function ProductsList() {
    const products = useSelector(state => state.products.items);
    const context = useContext(ToggleLayoutContext);
    const inlineLayout = context.inlineLayout;

    return (
        <>
            <LayoutChanger/>
            <div className="d-flex justify-content-between flex-wrap">
                {inlineLayout ? products.map(product => {
                    return <CardInline
                        key={product.id}
                        product={product}
                    />
                }) : products.map(product => {
                    return <ProductCard
                        key={product.id}
                        product={product}
                    />
                })
                }
            </div>
        </>
    )
}

export default ProductsList;