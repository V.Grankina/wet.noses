import React from "react";
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {useDispatch, useSelector} from "react-redux";
import {addFavoriteProduct, removeFavoriteProduct} from "../../state/actions/favoriteProducts";
import {modalClose, modalOpen} from "../../state/actions/modal";
import {addToCart, plusProduct} from "../../state/actions/cart";

function ProductCardInline({product}) {
    const dispatch = useDispatch();
    const favoriteProducts = useSelector(state => state.favoriteProducts.items);
    const addedToCart = useSelector(state => state.cart.items);
    let liked = false;
    if (favoriteProducts.some(id => id === product.id)) {
        liked = true;
    }
    const onAddToFavorite = ({id}) => {
        if (favoriteProducts.some(item => item === id)) {
            dispatch(removeFavoriteProduct(id))
        } else {
            dispatch(addFavoriteProduct(id))
        }
    }
    const onAddToCart = (product) => {
        if (addedToCart.some(({id}) => id === product.id)) {
            dispatch(plusProduct(product.id))
        } else {
            dispatch(addToCart(product.id))
        }
        dispatch(modalClose());
    }
    const onOpenModal = (product) => {
        dispatch(modalOpen("Do you want to add this beautiful fountain to cart?",
            () => {
                onAddToCart(product)
            }, product));
    }
    return (
        <Card className="product-card m-3 d-flex flex-lg-row w-100">
            <div className="d-flex flex-column justify-content-between">
                {liked === true ? <img src={process.env.PUBLIC_URL + '/images/icons8-favorite-30.png'}
                                       width="30px"
                                       height="30px"
                                       onClick={() => onAddToFavorite(product)}
                                       alt=""/> :
                    <img src={process.env.PUBLIC_URL + '/images/icons8-favorite-40.png'}
                         width="30px"
                         height="30px"
                         onClick={() => onAddToFavorite(product)}
                         alt=""
                    />}
                <img src={process.env.PUBLIC_URL + `${product.image}`}
                     width="200px"
                     height="auto"
                     alt=""
                />
            </div>
            <Card.Body className="d-flex justify-content-between">
                <div className="d-flex flex-column justify-content-around">
                <Card.Title>{product.name}</Card.Title>
                <Card.Text>
                    Vendor-code: {product.vendor}
                </Card.Text>
                <Card.Text className="text-danger">
                    Price: {product.price} USD
                </Card.Text>
                </div>
                <Button variant="info" className='align-self-end' onClick={() => onOpenModal(product)}>Add to cart</Button>
            </Card.Body>
        </Card>
    );
}


export default ProductCardInline;
