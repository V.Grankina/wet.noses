import ProductCard from './Card';
import renderer from 'react-test-renderer';
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";
import {render, screen} from "@testing-library/react";
import '@testing-library/jest-dom';

const mockStore = configureMockStore([thunk]);

const product = {
    "id": 1,
    "name": "Ciays Water Fountain",
    "price": "50",
    "image": "/images/1.jpg",
    "vendor": "341524909",
    "color": "Gray"
}

describe('Product Card tests', ()=>{
    it('renders correctly', ()=>{
        const testStore = mockStore({
            favoriteProducts: {
                items: [],
            },
            cart: {
                items: [],
            }
        });
        const tree = renderer
            .create(<Provider store={testStore}><ProductCard product={product}/></Provider>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })
    it ('should have product name', ()=>{
        const testStore = mockStore({
            favoriteProducts: {
                items: [],
            },
            cart: {
                items: [],
            }
        });
        render(<Provider store={testStore}><ProductCard product={product}/></Provider>);
        const productName = screen.getByText(/Ciays Water Fountain/i);
        expect(productName).toBeInTheDocument()
    })
})

