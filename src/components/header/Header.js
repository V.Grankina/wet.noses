import React from "react";
import { Link, NavLink } from "react-router-dom";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Cart from "../Cart";
import Favorite from "../Favorite";

function Header (){
    return (
        <header className='header sticky-top'>
            <Navbar bg='info' expand="lg" variant="dark">
                <Container className="d-flex justify-content-between">
                    <Link to="/home" className="text-light" style={{fontSize: 35, textDecoration:"none"}}>
                        <img
                            src={process.env.PUBLIC_URL + '/images/icons8-pets-64.png'}
                            width="50"
                            height="50"
                            className="d-inline-block align-top"
                            alt="React Bootstrap logo"
                        />{' '}
                        WET.noses
                    </Link>
                    <Nav>
                        <NavLink className="nav-item nav-link" to="/home">Home</NavLink>
                        <NavLink className="nav-item nav-link" to="/cart">Cart</NavLink>
                        <NavLink className="nav-item nav-link" to="/favorite">Favorite</NavLink>
                    </Nav>
                    <Nav>
                        <Cart className="mr-auto p-2"/>
                        <Favorite/>
                    </Nav>
                </Container>
            </Navbar>
        </header>
    )
}

 export default Header;