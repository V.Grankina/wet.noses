import React from "react";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Navbar from "react-bootstrap/Navbar";
import {useDispatch, useSelector} from "react-redux";
import {modalClose} from "../state/actions/modal";

function ModalWindow() {
    const dispatch = useDispatch();
    const isOpen = useSelector((state)=> state.modal.isOpen);
    const onConfirm = useSelector((state)=> state.modal.confirmFn);
    const text = useSelector((state)=> state.modal.text);
    const onClose = () => {
        dispatch(modalClose())
    }
    return (
        <Modal show={isOpen} onHide={onClose} data-testid='modal'>
            <Modal.Header closeButton>
                <Modal.Title><Navbar.Brand href="/home" style={{fontSize: 35}}>
                    <img
                        src={process.env.PUBLIC_URL + '/images/icons8-pets-64.png'}
                        width="50"
                        height="50"
                        className="d-inline-block align-top"
                        alt="React Bootstrap logo"
                    />{' '}
                    WET.noses
                </Navbar.Brand>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>{text}</Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onClose}>
                    No
                </Button>
                <Button variant="info" onClick={onConfirm}>
                    Yes
                </Button>
            </Modal.Footer>
        </Modal>
    );
}


export default ModalWindow;