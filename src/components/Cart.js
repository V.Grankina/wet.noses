import React from "react";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

function Cart() {
    const count = useSelector(state => state.cart.items.length)
    return (<Link to="/cart" style={{textDecoration: "none", color: "white"}}>
            <img
                src={process.env.PUBLIC_URL + '/images/icons8-shopping-cart-64.png'}
                width="50"
                height="50"
                className="d-inline-block align-top"
                alt=""
            />
            {count}
        </Link>
    )
}

export default Cart;