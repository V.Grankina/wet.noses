import {MODAL_WINDOW_CLOSE, MODAL_WINDOW_OPEN} from "../actions/modal";

const initialState = {
    isOpen: false,
    text: "",
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case MODAL_WINDOW_OPEN:
            return {...state, isOpen: true, ...action.payload}
        case MODAL_WINDOW_CLOSE:
            return {...state, isOpen: false}
        default:
            return state
    }
}