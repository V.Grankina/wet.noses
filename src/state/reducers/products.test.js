import {reducer} from './products';
import {FETCH_PRODUCTS_BEGIN, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR} from "../actions/products";

describe('products reducer tests', () => {
    it('should start fetching', () => {
        const initialState = {
            items: [],
            isFetching: false,
            isError: false,
            error: null
        }
        const action = {
            type: FETCH_PRODUCTS_BEGIN
        }
        const newState = reducer(initialState, action);
        expect(newState.isFetching).toBe(true);
    })
    it('should generate Error', () => {
        const initialState = {
            items: [],
            isFetching: false,
            isError: false,
            error: null
        }
        const action = {
            type: FETCH_PRODUCTS_ERROR,
            payload: new Error('Test Error')
        }
        const newState = reducer(initialState, action);
        expect(newState.error.message).toBe('Test Error');
    })
    it('should add two items', () => {
        const initialState = {
            items: [],
            isFetching: true,
            isError: false,
            error: null
        }
        const action = {
            type: FETCH_PRODUCTS_SUCCESS,
            payload: [{item: 1}, {item: 2}]
        }
        const newState = reducer(initialState, action);
        expect(newState.items).toHaveLength(2)
    })
})