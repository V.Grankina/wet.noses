import {FETCH_PRODUCTS_BEGIN, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR} from "../actions/products";

const initialState = {
    items: [],
    isFetching: false,
    isError: false,
    error: null
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_BEGIN:
            return {...state, isFetching: true}
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, items: action.payload, isFetching: false}
        case FETCH_PRODUCTS_ERROR:
            return {...state, error: action.payload, isError: true}
        default:
            return state
    }
}