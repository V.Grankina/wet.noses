import {SET_CART, ADD_TO_CART, REMOVE_FROM_CART, MINUS_PRODUCT, PLUS_PRODUCT, CLEAR_CART} from "../actions/cart";

const initialState = {
    items: [],
}
export const reducer = (state = initialState, action) => {
    let newItems;
    switch (action.type) {
        case SET_CART:
            return {items: action.payload};
        case ADD_TO_CART:
            newItems = [...state.items, {id: action.payload, count: 1}];
            localStorage.setItem('addedToCart', JSON.stringify(newItems));
            return {items: newItems};
        case REMOVE_FROM_CART:
            newItems = state.items.filter(({id}) => id !== action.payload);
            localStorage.setItem('addedToCart', JSON.stringify(newItems));
            return {items: newItems};
        case PLUS_PRODUCT:
            newItems = state.items.map(product => {
                if (product.id === action.payload) {
                    return {...product, count: product.count + 1}
                } else {
                    return product
                }
            });
            localStorage.setItem('addedToCart', JSON.stringify(newItems));
            return {items: newItems};
        case MINUS_PRODUCT:
            newItems = state.items.map(product => {
                if (product.id === action.payload) {
                    return {...product, count: product.count - 1}
                } else {
                    return product
                }
            });
            localStorage.setItem('addedToCart', JSON.stringify(newItems));
            return {items: newItems};
        case CLEAR_CART:
            localStorage.removeItem('addedToCart');
            return {items: []};
        default:
            return state
    }
}