import {FAVORITE_PRODUCTS_ADD, FAVORITE_PRODUCTS_REMOVE, FAVORITE_PRODUCTS_SET} from "../actions/favoriteProducts";

const initialState = {
    items: []
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FAVORITE_PRODUCTS_SET:
            return {items: action.payload};
        case FAVORITE_PRODUCTS_ADD:
            const newItems = [...state.items, action.payload];
            localStorage.setItem('favoriteProducts', JSON.stringify(newItems));
            return {items: newItems};
        case FAVORITE_PRODUCTS_REMOVE:
            const index = state.items.findIndex(value => value === action.payload);
            const newItems1 = [...state.items.slice(0, index), ...state.items.slice(index + 1)];
            localStorage.setItem('favoriteProducts', JSON.stringify(newItems1));
            return {items: newItems1};
        default:
            return state
    }
}