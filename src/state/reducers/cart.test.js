import {reducer} from './cart';
import {SET_CART, ADD_TO_CART, REMOVE_FROM_CART, MINUS_PRODUCT, PLUS_PRODUCT, CLEAR_CART} from "../actions/cart";

describe('test cart reducer', () => {
    it('should add all items', ()=>{
        const initialState = {
            items: [],
        };
        const action = {
            type: SET_CART,
            payload: [1,2,3,4]
        }
        const newState = reducer(initialState, action);
        expect(newState.items).toHaveLength(4)
    })
    it('should add one item', () => {
        const initialState = {
            items: [],
        };
        const action = {
            type: ADD_TO_CART,
            payload: 1
        }
        const newState = reducer(initialState, action);
        expect(newState.items).toHaveLength(1)
    })
    it('should remove one  item from cart', () => {
        const initialState = {
            items: [{id: 1, count: 1},{id: 2, count: 2}],
        };
        const action = {
            type: REMOVE_FROM_CART,
            payload: 1
        }
        const newState = reducer(initialState, action);
        expect(newState.items).toHaveLength(1)
    })
    it('should minus one item in cart', () => {
        const initialState = {
            items: [{id: 2, count: 2}],
        };
        const action = {
            type: MINUS_PRODUCT,
            payload: 2
        }
        const newState = reducer(initialState, action);
        expect(newState.items[0].count).toBe(1)
    })
    it('should plus one item in cart', () => {
        const initialState = {
            items: [{id: 2, count: 2}],
        };
        const action = {
            type: PLUS_PRODUCT,
            payload: 2
        }
        const newState = reducer(initialState, action);
        expect(newState.items[0].count).toBe(3)
    })
    it('should clear cart', () => {
        const initialState = {
            items: [{id: 2, count: 2}, {id: 3, count: 2}],
        };
        const action = {
            type: CLEAR_CART,
            payload: 2
        }
        const newState = reducer(initialState, action);
        expect(newState.items).toHaveLength(0)
    })
})