import {reducer} from './favoriteProducts';
import {FAVORITE_PRODUCTS_ADD, FAVORITE_PRODUCTS_REMOVE, FAVORITE_PRODUCTS_SET} from "../actions/favoriteProducts";

describe('favorite products reduser tests', ()=>{
    it('should add all items', ()=>{
        const initialState = {
            items: [],
        };
        const action = {
            type: FAVORITE_PRODUCTS_SET,
            payload: [1,2,3,4]
        }
        const newState = reducer(initialState, action);
        expect(newState.items).toHaveLength(4)
    })
    it('should add one item', () => {
        const initialState = {
            items: [],
        };
        const action = {
            type: FAVORITE_PRODUCTS_ADD,
            payload: 1
        }
        const newState = reducer(initialState, action);
        expect(newState.items).toHaveLength(1)
    })
    it('should remove one  item from cart', () => {
        const initialState = {
            items: [1,2],
        };
        const action = {
            type: FAVORITE_PRODUCTS_REMOVE,
            payload: 1
        }
        const newState = reducer(initialState, action);
        expect(newState.items).toHaveLength(1)
    })
})