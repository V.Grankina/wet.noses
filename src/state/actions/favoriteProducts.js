export const FAVORITE_PRODUCTS_ADD = "FAVORITE_PRODUCTS_ADD";
export const FAVORITE_PRODUCTS_REMOVE = "FAVORITE_PRODUCTS_REMOVE";
export const FAVORITE_PRODUCTS_SET = "FAVORITE_PRODUCTS_SET";

export const addFavoriteProduct = (id) => ({type: FAVORITE_PRODUCTS_ADD, payload: id});
export const removeFavoriteProduct = (id) => ({type: FAVORITE_PRODUCTS_REMOVE, payload: id});
export const setFavoriteProducts = (items) => ({type: FAVORITE_PRODUCTS_SET, payload: items});
