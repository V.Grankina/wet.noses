export const SET_CART = 'SET_CART';
export const CLEAR_CART = 'CLEAR_CART';
export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const PLUS_PRODUCT = "PLUS_PRODUCT";
export const MINUS_PRODUCT = "MINUS_PRODUCT";

export const setCart = (items) => ({type: SET_CART, payload: items});
export const addToCart = (id) => ({type: ADD_TO_CART, payload: id});
export const removeFromCart = (id) => ({type: REMOVE_FROM_CART, payload: id});
export const plusProduct = (id) => ({type: PLUS_PRODUCT, payload: id});
export const minusProduct = (id) => ({type: MINUS_PRODUCT, payload: id});
export const clearCart = () => ({type: CLEAR_CART})