export const FETCH_PRODUCTS_BEGIN = "FETCH_PRODUCTS_BEGIN";
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTS_ERROR = "FETCH_PRODUCTS_ERROR";


export const fetchProductsBegin = () => ({type: FETCH_PRODUCTS_BEGIN});
export const fetchProductsSuccess = (data) => ({type: FETCH_PRODUCTS_SUCCESS, payload: data})
export const fetchProductsError = (err) => ({type: FETCH_PRODUCTS_ERROR, payload: err});


export const fetchProducts = () => {
    return (dispatch) => {
        dispatch(fetchProductsBegin())
        fetch(`fountains.json`)
            .then(response => response.json())
            .then(data => dispatch(fetchProductsSuccess(data)))
            .catch(err => dispatch(fetchProductsError(err)))
    }
}